package main

import (
	"context"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/alecthomas/kingpin/v2"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"gitlab.com/gitlab-org/quality/geoctl/pkg/geoctl" //nolint:typecheck
)

var (
	bucketCmd    = kingpin.Command("buckets", "Run commands against Geo buckets.")
	bucketConfig = bucketCmd.Flag("config", "Path to config file").
			Envar("GEOCTL_CONFIG").Required().String()
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-sigs
		cancel()
	}()

	logger := log.NewLogfmtLogger(log.NewSyncWriter(os.Stdout))
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	logger = level.NewInjector(logger, level.InfoValue())

	if kingpin.Parse() == "buckets" {
		fatal(logger, geoctl.Main(ctx, *bucketConfig, logger))
	}
}

func fatal(logger log.Logger, err error) {
	if err != nil {
		level.Error(logger).Log("event", "shutdown", "error", err)
		if strings.Contains(err.Error(), "context canceled") {
			os.Exit(0)
		}
		os.Exit(1)
	}
}
