package geoctl

import (
	"context"
	"encoding/csv"
	"fmt"
	"os"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	awsConfig "github.com/aws/aws-sdk-go-v2/config"
	awsS3 "github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/go-kit/log/level"
)

type bucketObject struct {
	Key                   string
	FileSize              string
	PrimaryLastModified   string
	SecondaryLastModified string
}

type replicationTimes struct {
	ReplicationTime     float64
	NumberOfOccurrences float64
}

type summaryStats struct {
	ObjectCount             int
	TotalReplicationLag     time.Duration
	ShortestReplicationTime time.Duration
	LongestReplicationTime  time.Duration
}

func verifyReplication(args *Args) error {
	var (
		errors              int
		csvReplicationTimes []replicationTimes
		bucketObjects       []bucketObject
	)

	stats := summaryStats{
		ObjectCount:             0,
		TotalReplicationLag:     time.Duration(0),
		ShortestReplicationTime: time.Duration(1000000),
		LongestReplicationTime:  time.Duration(0),
	}

	awsPrimaryClient, err := configureAWS(args.Context, args.Config.AWS.PrimaryRegion)
	if err != nil {
		return err
	}

	awsSecondaryClient, err := configureAWS(args.Context, args.Config.AWS.SecondaryRegion)
	if err != nil {
		return err
	}

	primaryBucket, err := awsPrimaryClient.ListObjectsV2(context.TODO(), &awsS3.ListObjectsV2Input{
		Bucket: aws.String(args.Config.Buckets.PrimaryBucket),
	})
	if err != nil {
		return err
	}

	for _, primaryObject := range primaryBucket.Contents {
		primaryModified := primaryObject.LastModified
		secondaryBucket, err := awsSecondaryClient.ListObjectsV2(context.TODO(), &awsS3.ListObjectsV2Input{
			Bucket: aws.String(args.Config.Buckets.SecondaryBucket),
			Prefix: primaryObject.Key,
		})
		if err != nil {
			return err
		}

		if len(secondaryBucket.Contents) > 1 {
			level.Error(args.Logger).Log("event", "Found to many objects", "object", primaryObject.Key)
		}

		var replicationTime time.Duration
		for _, secondaryObject := range secondaryBucket.Contents {
			secondaryModified := secondaryObject.LastModified

			replicationTime = secondaryModified.Sub(*primaryModified)
			if replicationTime < time.Duration(0) {
				errors++
				level.Error(args.Logger).Log("event", "Negative modified diff",
					"primaryObject", primaryObject.Key, "primaryModified", primaryObject.LastModified,
					"secondaryObject", secondaryObject.Key, "secondaryModified", secondaryObject.LastModified)
			}

			stats.ObjectCount++
			stats.TotalReplicationLag += replicationTime
			csvReplicationTimes = countReplicationTime(csvReplicationTimes, replicationTime.Seconds())
			bucketObjects = append(bucketObjects, bucketObject{
				Key:                   fmt.Sprintf("%s", aws.ToString(primaryObject.Key)),
				FileSize:              fmt.Sprintf("%d", primaryObject.Size),
				PrimaryLastModified:   fmt.Sprint(primaryObject.LastModified),
				SecondaryLastModified: fmt.Sprint(secondaryObject.LastModified),
			})

			if replicationTime < stats.ShortestReplicationTime {
				stats.ShortestReplicationTime = replicationTime
			} else if replicationTime > stats.LongestReplicationTime {
				stats.LongestReplicationTime = replicationTime
			}
		}
	}
	level.Info(args.Logger).Log("TotalObjectsCounted", stats.ObjectCount, "errors", errors)

	level.Info(args.Logger).Log("event", "writing results to results.csv")
	err = writeCSV(stats, csvReplicationTimes, bucketObjects)
	if err != nil {
		return err
	}

	return nil
}

func writeCSV(stats summaryStats, replicationTimes []replicationTimes, bucketObjects []bucketObject) error {
	csvFile, err := os.Create("results.csv")
	if err != nil {
		return err
	}
	defer csvFile.Close()

	w := csv.NewWriter(csvFile)
	defer w.Flush()

	summaryHeaderRow := []string{"Total Objects Counted", "Average Replication Time (s)", "Shortest Replication Time (s)", "Longest Replication Time (s)"}
	err = w.Write(summaryHeaderRow)
	if err != nil {
		return err
	}

	summaryRow := []string{fmt.Sprint(stats.ObjectCount), fmt.Sprint(stats.TotalReplicationLag.Seconds() / float64(stats.ObjectCount)),
		fmt.Sprint(stats.ShortestReplicationTime.Seconds()), fmt.Sprint(stats.LongestReplicationTime.Seconds())}
	err = w.Write(summaryRow)
	if err != nil {
		return err
	}

	headerRow := []string{"Replication Time (s)", "Number Of Occurrences"}
	err = w.Write(headerRow)
	if err != nil {
		return err
	}

	for _, csvRow := range replicationTimes {
		row := []string{fmt.Sprint(csvRow.ReplicationTime), fmt.Sprint(csvRow.NumberOfOccurrences)}
		err = w.Write(row)
		if err != nil {
			return err
		}
	}

	headerRow = []string{"Object Key", "Object Size", "Last Modified on Primary", "Last Modified on Secondary"}
	err = w.Write(headerRow)
	if err != nil {
		return err
	}

	for _, csvRow := range bucketObjects {
		row := []string{csvRow.Key, csvRow.FileSize, csvRow.PrimaryLastModified, csvRow.SecondaryLastModified}
		err = w.Write(row)
		if err != nil {
			return err
		}
	}

	return nil
}

func configureAWS(ctx context.Context, region string) (*awsS3.Client, error) {
	cfg, err := awsConfig.LoadDefaultConfig(ctx, awsConfig.WithRegion(region))
	if err != nil {
		return nil, err
	}

	return awsS3.NewFromConfig(cfg), nil
}

func countReplicationTime(slice []replicationTimes, key float64) []replicationTimes {
	index := -1
	for i, element := range slice {
		if element.ReplicationTime == key {
			index = i
			break
		}
	}

	if index >= 0 {
		rt := &slice[index]
		rt.NumberOfOccurrences++
		return slice
	}

	return append(slice, replicationTimes{
		ReplicationTime:     key,
		NumberOfOccurrences: 1,
	})
}
