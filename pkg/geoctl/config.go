package geoctl

import (
	"os"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"gopkg.in/yaml.v3"
)

type Config struct {
	CloudProvider string       `yaml:"cloud_provider"`
	AWS           AWSConfig    `yaml:"aws"`
	Buckets       BucketConfig `yaml:"buckets"`
}

type AWSConfig struct {
	PrimaryRegion   string `yaml:"primary_region"`
	SecondaryRegion string `yaml:"secondary_region"`
}

type BucketConfig struct {
	PrimaryBucket   string `yaml:"primary_bucket"`
	SecondaryBucket string `yaml:"secondary_bucket"`
}

func loadConfig(configFile string, logger log.Logger) (*Config, error) {
	cfg := Config{}
	configLogger := log.With(logger, "event", "loadConfig", "config_path", "configFile")

	level.Info(configLogger).Log("status", "Loading Config")
	data, err := os.ReadFile(configFile)
	if err != nil {
		level.Error(configLogger).Log("status", "error", "err", err)
		return &cfg, err
	}

	err = yaml.Unmarshal(data, &cfg)
	if err != nil {
		level.Error(configLogger).Log("status", "error", "err", err)
		return &cfg, err
	}

	level.Info(configLogger).Log("status", "Config Loaded")
	return &cfg, nil
}
