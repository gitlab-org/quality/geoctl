package geoctl

import (
	"context"

	"github.com/go-kit/log"
)

type Args struct {
	Context context.Context
	Config  *Config
	Logger  log.Logger
}

func Main(ctx context.Context, configFile string, logger log.Logger) error {
	cfg, err := loadConfig(configFile, logger)
	if err != nil {
		return err
	}

	args := Args{
		Context: ctx,
		Logger:  logger,
		Config:  cfg,
	}

	err = verifyReplication(&args)
	if err != nil {
		return err
	}

	return nil
}
