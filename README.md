Compare the Last Modified date for all items in 2 AWS buckets. Authentication for the buckets should be set up separately.

## Run

go run ./cmd/geoctl buckets --config ./configs/config.yml

## Results

Once complete the program will create a `results.csv` file that can be viewed in Sheets.

```text
Total Objects Counted Average Replication Time (s) Shortest Replication Time (s) Longest Replication Time (s)
1000                  1.579                        0                             7                           

Replication Time (s)	Number Of Occurrences		
1	                    469		
2	                    369		
0	                    48		
3	                    95		
4	                    11		
5	                    6		
7	                    1		
6	                    1		

Object Key	    Object Size	    Last Modified on Primary	    Last Modified on Secondary
0x1400004c080	0x14000010398	2024-01-17 14:07:20 +0000 UTC	2024-01-17 14:07:21 +0000 UTC
0x14000215e20	0x140001d1998	2024-01-17 14:06:42 +0000 UTC	2024-01-17 14:06:44 +0000 UTC
0x14000215e90	0x140001d1b48	2024-01-17 14:06:35 +0000 UTC	2024-01-17 14:06:37 +0000 UTC
0x1400053a270	0x1400052c5f8	2024-01-17 14:07:32 +0000 UTC	2024-01-17 14:07:32 +0000 UTC
0x14000110320	0x140001d1e18	2024-01-17 14:07:03 +0000 UTC	2024-01-17 14:07:05 +0000 UTC
0x1400053a6e0	0x1400052c8b8	2024-01-17 14:06:23 +0000 UTC	2024-01-17 14:06:24 +0000 UTC
0x14000110900	0x1400014e108	2024-01-17 14:06:50 +0000 UTC	2024-01-17 14:06:51 +0000 UTC
0x1400053ab50	0x1400052cb78	2024-01-17 14:05:40 +0000 UTC	2024-01-17 14:05:41 +0000 UTC
0x140001cc350	0x1400014e528	2024-01-17 14:07:14 +0000 UTC	2024-01-17 14:07:15 +0000 UTC
0x1400053afc0	0x1400052ce38	2024-01-17 14:06:27 +0000 UTC	2024-01-17 14:06:28 +0000 UTC
0x140001cc7c0	0x1400014e8c8	2024-01-17 14:05:49 +0000 UTC	2024-01-17 14:05:50 +0000 UTC
0x1400053b430	0x1400052d0f8	2024-01-17 14:06:16 +0000 UTC	2024-01-17 14:06:18 +0000 UTC
0x140001ccc30	0x1400014ec48	2024-01-17 14:07:31 +0000 UTC	2024-01-17 14:07:32 +0000 UTC
...
```